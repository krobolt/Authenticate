<?php

namespace Tests\Authenticate;

use Ds\Authenticate\Token;

/**
 * Class TokenTest
 *
 * @package Tests\Authenticate
 */
class TokenTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Token
     */
    public $token;

    /**
     *
     */
    protected function setUp()
    {
        $this->token = new Token();
    }

    /**
     * Test token randomness.
     */
    public function testTokenRandomness()
    {
        $tokens = [];
        for ($generateToken = 0; $generateToken < 100; $generateToken++) {
            $tokens[] = $this->token->create(40);
        }
        $duplicates = array_diff_assoc($tokens, array_unique($tokens));
        $this->assertEquals($duplicates, array());
    }

    /**
     *
     */
    public function testCreateFromString()
    {
        $string = 'foobar';
        $key = 'a-private-key';
        $algo = 'sha256';
        $expected = bin2hex(hash_hmac($algo, $string, $key));

        $actual = $this->token->createFromString($string, $key, $algo);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @param $length
     * @dataProvider tokenLengthProvider
     */
    public function testCreate($length)
    {
        $token = $this->token->create($length);
        $this->assertEquals(strlen($token), $length);
    }

    /**
     * @return array
     */
    public function tokenLengthProvider()
    {
        return array([40], [80], [100], [150], [130], [150], [200], [300]);
    }
}
